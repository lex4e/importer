<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "import_process".
 *
 * @property int $id
 * @property string $filename
 * @property string|null $created
 * @property string $status
 * @property string $store
 * @property int $number_files
 */
class ImportProcess extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'NEW';
    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_DONE = 'DONE';
    const STATUS_ERROR = 'ERROR';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'import_process';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['filename', 'status'], 'required'],
            [['created', 'number_files'], 'safe'],
            [['filename', 'store'], 'string', 'max' => 127],
            [['status'], 'string', 'max' => 63],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'created' => 'Created',
            'status' => 'Status',
            'store' => 'Store',
            'number_files' => 'Number files',
        ];
    }

    /**
     * @param $filename
     * @param $storeName
     */
    public function create($filename, $storeName) {
        $this->filename = $filename;
        $this->store = $storeName ?? '';
        $this->number_files = 0;
        $this->change(self::STATUS_NEW);
    }

    public function start() {
        $this->change(self::STATUS_PROCESSING);
    }

    public function done($numberFiles) {
        $this->number_files = $numberFiles;
        $this->change(self::STATUS_DONE);
    }

    public function error() {
        $this->change(self::STATUS_ERROR);
    }

    public function change($status) {
        $this->created = date('Y-m-d', time());
        $this->status = $status;

        $this->save(false);
    }
}
