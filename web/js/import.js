const max_number_files = 2;
let tableResults = document.getElementById('tableResults');

let pushData = (fileType, content, fileName, storeId) => {
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/importer/api/product/import", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let response = this.responseText;
            response = JSON.parse(response);
            tableResults.innerHTML = tableResults.innerHTML +
                messageTemplate(response.status, fileName, response.message);
            ;
        }
    };
    let data = { ext: fileType, content: content, file: fileName, store: storeId };
    xhttp.send(JSON.stringify(data));
}

let messageTemplate = (status, fileName, message) => {
    return  `<tr class="${status}">
                <td>Filename: ${fileName}</td>
                <td>Status: ${status}</td>
                <td>Message: ${message}</td>
            </tr>`;
}

document.getElementById("import_file").onchange = function(){
    let files = document.getElementById("import_file").files;
    let storeId = document.getElementById('store').value;
    if (files.length > max_number_files) {
        tableResults.innerHTML = tableResults.innerHTML +
            messageTemplate('error', '',
                `Message: Upload more than ${max_number_files} files`);
        return;
    }
    if (!(parseInt(storeId) > 0)) {
        tableResults.innerHTML = tableResults.innerHTML +
            messageTemplate('error', '',
                `Message: Undefined Store`);
        return;
    }
    let promises = [];
    for (let index in files) {
        try {
            let file = files[index];
            if (file && typeof file === 'object') {
                if (file.size > 5000000) {
                    tableResults.innerHTML = tableResults.innerHTML +
                        messageTemplate('error', file.name,
                            `Message: incorrect file size = ${file.size} (more than 5 Mb)`);
                    return;
                }
                promises.push(new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsText(file, "utf-8");
                    reader.addEventListener('load', (event) => {
                        let fileType = file.name.split('.')[file.name.split('.').length - 1];
                        let content = event.target.result;
                        pushData(fileType, content, file.name, storeId);
                    });
                    resolve();
                }));
            }
        } catch (e) {
            console.error(e);
        }
    }

    Promise.all(promises).then(result => {
        window.location.href = '/import-process';
    });
}
