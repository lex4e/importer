<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import Processes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-process-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'filename',
            'created',
            'status',
            'store',
            'number_files',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
