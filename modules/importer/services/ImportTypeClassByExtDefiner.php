<?php

namespace app\modules\importer\services;

class ImportTypeClassByExtDefiner
{
    /**
     * @param $ext
     * @return string
     * @throws \Exception
     */
    public static function getClassByExt($ext)
    {
        $class = sprintf("%s\%sImportService", __NAMESPACE__, ucfirst($ext));
        if (!class_exists($class))
            throw new \Exception("Import Type not define for file extension $ext", 400);

        return $class;
    }
}