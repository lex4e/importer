<?php

namespace app\modules\importer\services;

use yii\base\Model;

class EntitiesHandler
{
    /**
     * @param array $entities
     * @return array
     * @throws \Exception
     */
    public static function saveAll(array $entities)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($entities != null && is_array($entities)) {
                foreach ($entities as $entity) {
                    if ($entity != null && $entity instanceof Model) {
                        $entity->save();
                    }
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $entities;
    }
}
