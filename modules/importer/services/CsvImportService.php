<?php

namespace app\modules\importer\services;

use app\models\Store;
use app\models\StoreProduct;

/**
 * Class CsvImportService
 * @package app\modules\importer\services
 */
class CsvImportService implements ImportService
{
    /**
     * @param $content
     * @param $objectClass
     * @param $store
     * @return array
     */
    public function convertDataToObjects($content, $objectClass, $store)
    {
        $objects = [];
        $content = explode(PHP_EOL, $content);
        $keys = str_getcsv(reset($content), ',');
        for ($i = 1; $i < count($content); $i++) {
            if (!empty($content[$i]) && count($keys) === count(str_getcsv($content[$i], ','))) {
                $data = array_combine($keys, str_getcsv($content[$i], ','));
                /** @var StoreProduct $objectClass */
                $object = $objectClass::find()->where("upc = '{$data['upc']}'")->one();
                if (empty($object)) $object = new $objectClass();
                foreach ($data as $key => $value) {
                    $object->{$key} = $value;
                }
                $object->store_id = $store;
                $objects[] = $object;
            }
        }

        return $objects;
    }
}
