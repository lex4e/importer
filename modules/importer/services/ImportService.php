<?php

namespace app\modules\importer\services;

interface ImportService
{
    function convertDataToObjects($data, $objectClass, $store);
}