<?php

namespace app\modules\importer\controllers;

use app\models\Store;
use yii\web\Controller;

/**
 * Class ProductController
 * @package app\modules\importer\controllers
 */
class ProductController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'stores' => Store::find()->all()
        ]);
    }
}
