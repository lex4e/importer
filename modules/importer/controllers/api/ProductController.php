<?php

namespace app\modules\importer\controllers\api;

use app\models\ImportProcess;
use app\models\Store;
use app\models\StoreProduct;
use app\modules\importer\services\EntitiesHandler;
use app\modules\importer\services\ImportService;
use app\modules\importer\services\ImportTypeClassByExtDefiner;
use yii\web\Controller;

/**
 * Class ProductController
 * @package app\modules\importer\controllers\api
 */
class ProductController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return parent::beforeAction($action);
    }

    /**
     * @return array
     */
    public function actionImport()
    {
        $data = json_decode(\Yii::$app->request->getRawBody(), true);
        $importProcess = new ImportProcess();


        try {
            $store = Store::find()->where("[[id]] = '{$data['store']}'")->one();
            if (!$store instanceof Store) {
                throw new \Exception('Incorrect Store');
            }
            $importProcess->create($data['file'], $store->title);

            $importProcess->start();
            $importService = ImportTypeClassByExtDefiner::getClassByExt($data['ext']);
            $importServiceObject = new $importService();
            $products = $importServiceObject->convertDataToObjects($data['content'],
                StoreProduct::class, $data['store']);
            $products = EntitiesHandler::saveAll($products);
            $importProcess->done(count($products));

            return ['status' => 'success', 'message' => sprintf('Imported %d products', count($products)), 'content' => $products, 'file' => $data['file']];

        } catch (\Exception $e) {
            $importProcess->error();
            return ['status' => 'error', 'message' => $e->getMessage(), 'file' => $data['file']];
        }
    }
}
