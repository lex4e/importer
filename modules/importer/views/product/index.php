<?php $this->registerCssFile('/css/import.css'); ?>

<div class="importer-default-index">
    <h1>Import Products</h1>
    <form>
        <div class="form-group"><input type="file" id="import_file" multiple></div>
        <div class="form-group">
            <select id="store">
                <option disabled selected>Choice Store</option>
                <?php foreach ($stores as $store) : ?>
                    <option value="<?=$store->id?>"><?=$store->title?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </form>
    <table class="table" id="tableResults"></table>
</div>

<?php $this->registerJsFile('/js/import.js'); ?>
